const express = require('express');
const bodyParser = require('body-parser');

const promotionRouter = express.Router();

promotionRouter.use(bodyParser.json());

promotionRouter.route('/')
.all((req, res, next) => {
    res.statusCode = 200;
    res.setHeader("Content-Type",  'text/plain');
    next();
})
.get((req, res, next) => {
     res.end("Will send all promotion details");
})
.post((req, res, next) => {
    res.end("Will add promotion: " + req.body.name 
    + " with details: " + req.body.description);
})
.put((req, res, next) => {
    res.statusCode = 403;
    res.end("Put operation not supported on /promotions");
})
.delete((req, res, next) => {
    res.end('Deleting all the promotions!');
})

promotionRouter.route('/:promotionId')
.get((req, res, next) => {
    res.end("Will send you the details of promotion: " 
    + req.params.promotionId + " to you");
})
.post((req, res, next) => {
    res.statusCode = 403;
    res.end("Post operation not supported on /promotions/"
    + req.params.promotionId);
})
.put((req, res, next) => {
    res.write("Updating the promotion " + req.params.promotionId + "\n");
    res.end("Will updating the promotion: " + req.body.name
        + " with the details: " + req.body.description);
})
.delete((req, res, next) => {
    res.end("Will send you the details of promotion: " 
    + req.params.promotionId + " to you");
})

module.exports = promotionRouter;