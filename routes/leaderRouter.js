const express = require('express');
const bodyParser = require('body-parser');

const leaderRouter = express.Router();

leaderRouter.use(bodyParser.json());

leaderRouter.route('/')
.all((req, res, next) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})
.get((req, res, next) => {
    res.end("Will send all leader details");
})
.post((req, res, next) => {
    res.end("Will add leader: " + req.body.name 
    + " with details: " + req.body.description);
})
.put((req, res, next) => {
    res.statusCode = 200;
    res.end("Put operation not supported on /leaders");
})
.delete((req, res, next) => {
    res.end("Deleting all the leaders!");
});

leaderRouter.route('/:leaderId')
.get((req, res, next) => {
    res.end("Will send you the details of leader: " 
    + req.params.leaderId + " to you");
})
.post((req, res, next) => {
    res.statusCode = 403;
    res.end("Post operation not supported on /leaders/"
    + req.params.leaderId);
})
.put((req, res, next) => {
    res.write("Updating the leader " + req.params.leaderId + "\n");
    res.end("Will updating the leader: " + req.body.name
        + " with the details: " + req.body.description);
})
.delete((req, res, next) => {
    res.end("Will send you the details of leader: " 
    + req.params.leaderId + " to you");
});

module.exports = leaderRouter;